package ru.alexeyzhulin.bitcoinexchangerate.interfaces;

import ru.alexeyzhulin.bitcoinexchangerate.models.Currency;

/**
 * Created by Alex on 29.10.2017.
 */

public interface ICurrencyRefreshSubscriber {
    void refreshData();
    void setCurrency(Currency currency);
}
