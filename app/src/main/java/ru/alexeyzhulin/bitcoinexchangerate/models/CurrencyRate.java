package ru.alexeyzhulin.bitcoinexchangerate.models;

import java.util.Comparator;
import java.util.Date;

/**
 * Created by Alex on 19.10.2017.
 */

public class CurrencyRate {
    Currency currency;
    Number rateValue;
    String disclaimer;
    Date rateDate;
    ValueDirection valueDirection;
    Number delta;

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public Number getRateValue() {
        return rateValue;
    }

    public void setRateValue(Number rateValue) {
        this.rateValue = rateValue;
    }

    public String getDisclaimer() {
        return disclaimer;
    }

    public void setDisclaimer(String disclaimer) {
        this.disclaimer = disclaimer;
    }

    public Date getRateDate() {
        return rateDate;
    }

    public void setRateDate(Date rateDate) {
        this.rateDate = rateDate;
    }

    public ValueDirection getValueDirection() {
        return valueDirection;
    }

    public void setValueDirection(ValueDirection valueDirection) {
        this.valueDirection = valueDirection;
    }

    public Number getDelta() {
        return delta;
    }

    public void setDelta(Number delta) {
        this.delta = delta;
    }

    public static Comparator<CurrencyRate> currencyRateComparatorAsc = new Comparator<CurrencyRate>() {
        @Override
        public int compare(CurrencyRate val1, CurrencyRate val2) {
            return val1.getRateDate().compareTo(val2.getRateDate());
        }
    };

    public static Comparator<CurrencyRate> currencyRateComparatorDesc = new Comparator<CurrencyRate>() {
        @Override
        public int compare(CurrencyRate val1, CurrencyRate val2) {
            return val2.getRateDate().compareTo(val1.getRateDate());
        }
    };
}
