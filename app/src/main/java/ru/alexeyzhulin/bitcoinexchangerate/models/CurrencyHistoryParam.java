package ru.alexeyzhulin.bitcoinexchangerate.models;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by Alex on 30.10.2017.
 */

public class CurrencyHistoryParam {

    public static int LOAD_COUNT = 20;

    private Currency currency;
    private Date dateFrom;
    private Date dateTo;

    public CurrencyHistoryParam() {
        dateTo = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(dateTo);
        calendar.add(Calendar.DATE, -LOAD_COUNT);
        dateFrom = calendar.getTime();
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public Date getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(Date dateFrom) {
        this.dateFrom = dateFrom;
    }

    public Date getDateTo() {
        return dateTo;
    }

    public void setDateTo(Date dateTo) {
        this.dateTo = dateTo;
    }
}
