package ru.alexeyzhulin.bitcoinexchangerate.models;

import android.content.Context;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import ru.alexeyzhulin.bitcoinexchangerate.R;

import static android.support.v7.appcompat.R.attr.colorPrimary;


/**
 * Created by Alexey on 04.10.2017.
 */

public class CurrencyListAdapter extends ArrayAdapter<Currency> {
    // https://stackoverflow.com/questions/1625249/android-how-to-bind-spinner-to-custom-object-list

    // Your sent context
    private Context context;
    // Your custom values for the spinner (Zone)
    private Currency[] values;

    public CurrencyListAdapter(Context context, int textViewResourceId,
                               Currency[] values) {
        super(context, textViewResourceId, values);
        this.context = context;
        this.values = values;
    }

    public int getCount() {
        return values.length;
    }

    public Currency getItem(int position) {
        return values[position];
    }

    public long getItemId(int position) {
        return position;
    }


    // And the "magic" goes here
    // This is for the "passive" state of the spinner
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // I created a dynamic TextView here, but you can reference your own  custom layout for each spinner item
        TextView label = new TextView(context);
        //label.setTextColor(ContextCompat.getColor(context, R.color.colorPrimary));
        label.setTextColor(ContextCompat.getColor(context, R.color.colorPrimary));
        label.setTextSize(20);
        // Then you can get the current item using the values array (Zones array) and the current position
        // You can NOW reference each method you has created in your bean object (Zone class)
        label.setText(values[position].toString());

        // And finally return your dynamic (or custom) view for each spinner item
        return label;
    }

    // And here is when the "chooser" is popped up
    // Normally is the same view, but you can customize it if you want
    @Override
    public View getDropDownView(int position, View convertView,
                                ViewGroup parent) {
        TextView label = new TextView(context);
        //label.setTextColor(ContextCompat.getColor(context, R.color.colorPrimary));
        label.setTextColor(ContextCompat.getColor(context, R.color.colorPrimary));
        label.setTextSize(20);
        label.setText(values[position].toString());

        return label;
    }
}
