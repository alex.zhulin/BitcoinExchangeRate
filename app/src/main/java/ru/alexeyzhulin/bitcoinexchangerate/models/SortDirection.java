package ru.alexeyzhulin.bitcoinexchangerate.models;

/**
 * Created by Alex on 04.11.2017.
 */

public enum SortDirection {
    ASC, DESC
}
