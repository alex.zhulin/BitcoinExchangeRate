package ru.alexeyzhulin.bitcoinexchangerate.models;

/**
 * Created by Alexey on 02.11.2017.
 */

public enum ValueDirection {
    UP, DOWN, STABLE
}
