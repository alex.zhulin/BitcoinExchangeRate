package ru.alexeyzhulin.bitcoinexchangerate.services;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by Alexey on 24.09.2017.
 */

public class HttpService {

    public static final String API_BASE_URL = "https://api.coindesk.com/v1/bpi";

    private static final Integer URL_READ_TIMEOUT = 1000;
    private static final Integer URL_CONNECT_TIMEOUT = 3000;

    public static HttpURLConnection getUrlConnection(URL url) throws IOException {
        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
        urlConnection.setRequestMethod("GET");
        urlConnection.setReadTimeout(URL_READ_TIMEOUT);
        urlConnection.setConnectTimeout(URL_CONNECT_TIMEOUT);
        urlConnection.setDoOutput(true);

        return urlConnection;
    }
}
