package ru.alexeyzhulin.bitcoinexchangerate.services;

import android.util.JsonReader;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;

import ru.alexeyzhulin.bitcoinexchangerate.models.Currency;
import ru.alexeyzhulin.bitcoinexchangerate.models.CurrencyHistoryParam;
import ru.alexeyzhulin.bitcoinexchangerate.models.CurrencyRate;
import ru.alexeyzhulin.bitcoinexchangerate.models.SortDirection;
import ru.alexeyzhulin.bitcoinexchangerate.models.ValueDirection;

import static ru.alexeyzhulin.bitcoinexchangerate.models.SortDirection.DESC;

/**
 * Created by Alex on 19.10.2017.
 */

public class CurrencyHelper {

    private static String FILE_NAME_CURRENCY_DATA = "currency.ser";
    public static String USD_CURRENCY_CODE = "USD";
    public static String USD_CURRENCY_NAME = "United States Dollar";
    public static String BTC_CODE = "BTC";

    private static Currency createCurrencyFromJson(JsonReader jsonReader) throws IOException {
        Currency currency = new Currency();

        jsonReader.beginObject();
        while (jsonReader.hasNext()) {
            String name = jsonReader.nextName();
            if (name.equals("currency")) {
                currency.setCode(jsonReader.nextString());
            } else if (name.equals("country")) {
                currency.setName(jsonReader.nextString());
            } else {
                jsonReader.skipValue();
            }
        }
        jsonReader.endObject();


        return currency;
    }

    public static ArrayList<Currency> getCurrencyList() throws IOException {
        ArrayList<Currency> currencies = new ArrayList<>();

        URL url = new URL(HttpService.API_BASE_URL + "/supported-currencies.json");

        HttpURLConnection urlConnection = HttpService.getUrlConnection(url);
        urlConnection.connect();
        JsonReader jsonReader = new JsonReader(new InputStreamReader(url.openStream()));
        jsonReader.beginArray();
        while (jsonReader.hasNext()) {
            currencies.add(createCurrencyFromJson(jsonReader));
        }
        jsonReader.endArray();
        urlConnection.disconnect();

        return currencies;
    }

    public static CurrencyRate getCurrencyRate(Currency currency) throws IOException, JSONException, ParseException {
        CurrencyRate currencyRate = new CurrencyRate();

        URL url = new URL(HttpService.API_BASE_URL + "/currentprice/" + currency.getCode() + ".json");

        HttpURLConnection urlConnection = HttpService.getUrlConnection(url);
        urlConnection.connect();

        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(url.openStream()));
        String response = "";
        for (String line; (line = bufferedReader.readLine()) != null; response += line);
        JSONObject currencyRateAsJson = new JSONObject(response);
        currencyRate.setCurrency(currency);
        currencyRate.setDisclaimer(currencyRateAsJson.getString("disclaimer"));
        currencyRate.setRateValue(currencyRateAsJson.getJSONObject("bpi").getJSONObject(currency.getCode()).getDouble("rate_float"));

        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZZZZZ");
        currencyRate.setRateDate(dateFormat.parse(currencyRateAsJson.getJSONObject("time").getString("updatedISO")));

        urlConnection.disconnect();

        return currencyRate;
    }

    public static ArrayList<CurrencyRate> getCurrencyRateHistory(CurrencyHistoryParam currencyHistoryParam, SortDirection sortDirection) throws IOException, JSONException, ParseException {
        ArrayList<CurrencyRate> currencyRates = new ArrayList<>();

        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Currency currency = currencyHistoryParam.getCurrency();
        Date dateTo = currencyHistoryParam.getDateTo();
        Date dateFrom = currencyHistoryParam.getDateFrom();
        // Shift dateFrom for one day in order to calculate delta direction and value in all currencyHistoryParam period
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(dateFrom);
        calendar.add(Calendar.DATE, -1);
        dateFrom = calendar.getTime();
        URL url = new URL(HttpService.API_BASE_URL + "/historical/close.json?currency=" + currency.getCode() + "&start=" + dateFormat.format(dateFrom) + "&end=" + dateFormat.format(dateTo));

        HttpURLConnection urlConnection = HttpService.getUrlConnection(url);
        urlConnection.connect();

        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(url.openStream()));
        String response = "";
        for (String line; (line = bufferedReader.readLine()) != null; response += line);
        JSONObject currencyRateHistoryAsJson = new JSONObject(response);
        Iterator<String> currencyHistoryDates = currencyRateHistoryAsJson.getJSONObject("bpi").keys();
        while (currencyHistoryDates.hasNext()) {
            String currencyHistoryDate = currencyHistoryDates.next();
            CurrencyRate currencyRate = new CurrencyRate();
            currencyRate.setCurrency(currencyHistoryParam.getCurrency());
            currencyRate.setRateDate(dateFormat.parse(currencyHistoryDate));
            currencyRate.setRateValue(currencyRateHistoryAsJson.getJSONObject("bpi").getDouble(currencyHistoryDate));

            currencyRates.add(currencyRate);
        }
        // Sort collection in descending order
        if (sortDirection == DESC) {
            Collections.sort(currencyRates, CurrencyRate.currencyRateComparatorDesc);
        } else {
            Collections.sort(currencyRates, CurrencyRate.currencyRateComparatorAsc);
        }
        // Calculate delta direction and value
        for (int i = 0; i < currencyRates.size(); i++) {
            if (i < currencyRates.size() - 1) {
                CurrencyRate currencyRate = fillDeltaDirectionAndValue(currencyRates.get(i), currencyRates.get(i + 1));
                currencyRates.get(i).setValueDirection(currencyRate.getValueDirection());
                currencyRates.get(i).setDelta(currencyRate.getDelta());
            }
        }
        if (sortDirection == DESC) {
            // Remove last value
            currencyRates.remove(currencyRates.size() - 1);
        } else {
            // Remove first value
            currencyRates.remove(0);
        }

        urlConnection.disconnect();

        return currencyRates;
    }

    // Get next currency history param
    public static CurrencyHistoryParam getNextCurrencyHistoryParam(Currency currency, Date lastDate) {
        CurrencyHistoryParam nextCurrencyHistoryParam = new CurrencyHistoryParam();
        nextCurrencyHistoryParam.setCurrency(currency);
        // Next period calculation
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(lastDate);
        calendar.add(Calendar.DATE, -1);
        Date dateTo = calendar.getTime();

        calendar.setTime(dateTo);
        calendar.add(Calendar.DATE, -CurrencyHistoryParam.LOAD_COUNT);
        Date dateFrom = calendar.getTime();

        nextCurrencyHistoryParam.setDateFrom(dateFrom);
        nextCurrencyHistoryParam.setDateTo(dateTo);

        return nextCurrencyHistoryParam;
    }

    // Calculate currency delta direction and value
    private static CurrencyRate fillDeltaDirectionAndValue(CurrencyRate currentValue, CurrencyRate previousValue) {
        // Init returning value
        CurrencyRate currencyRate = new CurrencyRate();
        currencyRate.setCurrency(currentValue.getCurrency());
        currencyRate.setRateDate(currentValue.getRateDate());
        currencyRate.setRateValue(currentValue.getRateValue());
        // Fill extra params
        if (previousValue != null) {
            Number delta = (currentValue.getRateValue().floatValue() - previousValue.getRateValue().floatValue()) / previousValue.getRateValue().floatValue() * 100;
            if (delta.floatValue() > 0) {
                currencyRate.setValueDirection(ValueDirection.UP);
            } else if (delta.floatValue() < 0) {
                currencyRate.setValueDirection(ValueDirection.DOWN);
                delta = 0 - delta.floatValue();
            } else if (delta.floatValue() == 0) {
                currencyRate.setValueDirection(ValueDirection.STABLE);
            }
            currencyRate.setDelta(delta);
        }

        return currencyRate;
    }

    // Save user instance to file
    public static void saveCurrencyData(String saveDirectory, Currency currency) throws IOException {
        FileOutputStream myFileOutputStream = new FileOutputStream(saveDirectory + FILE_NAME_CURRENCY_DATA);
        ObjectOutputStream myObjectOutputStream = new ObjectOutputStream(myFileOutputStream);
        myObjectOutputStream.writeObject(currency);
        myObjectOutputStream.close();
    }

    // Load user instance from file
    public static Currency loadCurrencyData(String saveDirectory) throws IOException, ClassNotFoundException {
        FileInputStream myFileInputStream = new FileInputStream(saveDirectory + FILE_NAME_CURRENCY_DATA);
        ObjectInputStream myObjectInputStream = new ObjectInputStream(myFileInputStream);
        Currency userEntity = (Currency) myObjectInputStream.readObject();
        myObjectInputStream.close();
        return userEntity;
    }

}
