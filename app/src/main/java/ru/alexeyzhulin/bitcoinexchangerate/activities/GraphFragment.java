package ru.alexeyzhulin.bitcoinexchangerate.activities;

import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.utils.ColorTemplate;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import ru.alexeyzhulin.bitcoinexchangerate.R;
import ru.alexeyzhulin.bitcoinexchangerate.interfaces.ICurrencyRefreshSubscriber;
import ru.alexeyzhulin.bitcoinexchangerate.models.AsyncTaskResult;
import ru.alexeyzhulin.bitcoinexchangerate.models.Currency;
import ru.alexeyzhulin.bitcoinexchangerate.models.CurrencyHistoryParam;
import ru.alexeyzhulin.bitcoinexchangerate.models.CurrencyRate;
import ru.alexeyzhulin.bitcoinexchangerate.services.CurrencyHelper;

import static ru.alexeyzhulin.bitcoinexchangerate.models.SortDirection.ASC;
import static ru.alexeyzhulin.bitcoinexchangerate.models.SortDirection.DESC;

/**
 * Created by Alex on 04.11.2017.
 */

public class GraphFragment extends Fragment implements ICurrencyRefreshSubscriber {

    private View view;
    private Currency currency;
    private ProgressBar progressBar;
    private LineChart lineChart;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_graph, container, false);

        // Set listener for currency spinner change selection
        if(getActivity() instanceof MainActivity){
            ((MainActivity)getActivity()).setDataRefresher(this);
        }

        progressBar = view.findViewById(R.id.progressBar);
        progressBar.setVisibility(View.GONE);

        lineChart = view.findViewById(R.id.currencyGraph);

        Description description = new Description();
        description.setText("");
        lineChart.setDescription(description);

        XAxis xAxis = lineChart.getXAxis();
        xAxis.setTextColor(ContextCompat.getColor(view.getContext(), R.color.colorPrimary));
        xAxis.setValueFormatter(new IAxisValueFormatter() {

            private SimpleDateFormat mFormat = new SimpleDateFormat("dd MMM");

            @Override
            public String getFormattedValue(float value, AxisBase axis) {

                //long millis = TimeUnit.HOURS.toMillis((long) value);
                return mFormat.format(new Date((long) value));
            }
        });

        YAxis yAxis = lineChart.getAxisLeft();
        yAxis.setTextColor(ContextCompat.getColor(view.getContext(), R.color.colorPrimary));
        YAxis rightAxis = lineChart.getAxisRight();
        rightAxis.setEnabled(false);

        return view;
    }

    @Override
    public void refreshData() {
        CurrencyHistoryParam currencyHistoryParam = new CurrencyHistoryParam();
        currencyHistoryParam.setCurrency(currency);
        new getCurrencyRateHistory().execute(currencyHistoryParam);
    }

    @Override
    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    private class getCurrencyRateHistory extends AsyncTask<CurrencyHistoryParam, Integer, AsyncTaskResult<ArrayList<CurrencyRate>>> {

        @Override
        protected AsyncTaskResult<ArrayList<CurrencyRate>> doInBackground(CurrencyHistoryParam... currencyHistoryParams) {
            try {
                //progressBar.setVisibility(View.VISIBLE);
                return new AsyncTaskResult<>(CurrencyHelper.getCurrencyRateHistory(currencyHistoryParams[0], ASC));
            } catch (Exception e) {
                return new AsyncTaskResult<>(e);
            }
        }

        @Override
        protected void onPostExecute(AsyncTaskResult<ArrayList<CurrencyRate>> result) {
            if (result.getError() != null) {
                // error handling here
                //Toast.makeText(view.getContext(), R.string.connection_error, Toast.LENGTH_LONG).show();
                Toast.makeText(view.getContext(), result.getError().getMessage(), Toast.LENGTH_LONG).show();
            } else if (isCancelled()) {
                Toast.makeText(view.getContext(), R.string.request_canceled, Toast.LENGTH_LONG).show();
            } else {
                final ArrayList<CurrencyRate> currencyRates = result.getResult();
                ArrayList<Entry> values = new ArrayList<>();
                for (CurrencyRate currencyRate: currencyRates
                     ) {
                    values.add(new Entry(currencyRate.getRateDate().getTime(), currencyRate.getRateValue().floatValue()));
                }
                LineDataSet lineDataSet = new LineDataSet(values, "Bitcoin rate");
                lineDataSet.setColor(ContextCompat.getColor(view.getContext(), R.color.colorPrimary));
                lineDataSet.setCircleColor(ContextCompat.getColor(view.getContext(), R.color.colorPrimary));
                //lineDataSet.setValueTextColor(ContextCompat.getColor(view.getContext(), R.color.colorPrimary));
                lineDataSet.setLineWidth(3);
                LineData lineData = new LineData(lineDataSet);
                lineChart.setData(lineData);
                lineChart.invalidate();

            }
            progressBar.setVisibility(View.GONE);
        }
    }
}
