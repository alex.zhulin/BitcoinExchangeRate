package ru.alexeyzhulin.bitcoinexchangerate.activities;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.RotateAnimation;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.TimeZone;

import ru.alexeyzhulin.bitcoinexchangerate.R;
import ru.alexeyzhulin.bitcoinexchangerate.interfaces.ICurrencyRefreshSubscriber;
import ru.alexeyzhulin.bitcoinexchangerate.models.AsyncTaskResult;
import ru.alexeyzhulin.bitcoinexchangerate.models.Currency;
import ru.alexeyzhulin.bitcoinexchangerate.models.CurrencyRate;
import ru.alexeyzhulin.bitcoinexchangerate.services.CurrencyHelper;

/**
 * Created by Alex on 28.10.2017.
 */

public class CurrentRateFragment extends Fragment implements ICurrencyRefreshSubscriber {

    private ImageButton startButton;
    private RotateAnimation rotateAnimation;
    private TextView currencyRateTextView;
    private TextView currencyTextView;
    private TextView exchangeDateTextView;

    private View view;
    private Currency currency;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_current_rate, container, false);

        // Set listener for currency spinner change selection
        if(getActivity() instanceof MainActivity){
            ((MainActivity)getActivity()).setDataRefresher(this);
        }

        currencyRateTextView = view.findViewById(R.id.exchangeRate);
        currencyTextView = view.findViewById(R.id.exchangeCurrency);
        exchangeDateTextView = view.findViewById(R.id.exchangeDate);

        startButton = view.findViewById(R.id.imageButton);
        startButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startButton.startAnimation(rotateAnimation);
                new getCurrencyRate().execute(currency);
            }
        });

        rotateAnimation = (RotateAnimation) AnimationUtils.loadAnimation(view.getContext(), R.anim.rotate);

        return view;
    }

    @Override
    public void refreshData() {
        startButton.startAnimation(rotateAnimation);
        new getCurrencyRate().execute(currency);
    }

    @Override
    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    private class getCurrencyRate extends AsyncTask<Currency, Integer, AsyncTaskResult<CurrencyRate>> {

        @Override
        protected AsyncTaskResult<CurrencyRate> doInBackground(Currency... currencies) {
            try {
                return new AsyncTaskResult<>(CurrencyHelper.getCurrencyRate(currencies[0]));
            } catch (Exception e) {
                return new AsyncTaskResult<>(e);
            }
        }

        @Override
        protected  void onPostExecute(AsyncTaskResult<CurrencyRate> result) {
            if (result.getError() != null) {
                // error handling here
                //Toast.makeText(view.getContext(), R.string.connection_error, Toast.LENGTH_LONG).show();
                Toast.makeText(view.getContext(), result.getError().getMessage(), Toast.LENGTH_LONG).show();
            } else if (isCancelled()) {
                Toast.makeText(view.getContext(), R.string.request_canceled, Toast.LENGTH_LONG).show();
            } else {
                final CurrencyRate currencyRate = result.getResult();

                DecimalFormat formatter = new DecimalFormat("#,###.0000");
                DecimalFormatSymbols symbols = formatter.getDecimalFormatSymbols();
                symbols.setGroupingSeparator(' ');
                formatter.setDecimalFormatSymbols(symbols);

                currencyRateTextView.setText(formatter.format(currencyRate.getRateValue()));
                currencyTextView.setText(currencyRate.getCurrency().getCode() + " / " + CurrencyHelper.BTC_CODE);
                SimpleDateFormat dateFormat = new SimpleDateFormat("d MMM yyyy HH:mm:ss ZZZZ");
                dateFormat.setTimeZone(TimeZone.getDefault());
                exchangeDateTextView.setText(dateFormat.format(currencyRate.getRateDate()));
            }
            rotateAnimation.cancel();
            rotateAnimation.reset();
        }
    }
}
