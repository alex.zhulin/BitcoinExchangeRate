package ru.alexeyzhulin.bitcoinexchangerate.activities;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

import ru.alexeyzhulin.bitcoinexchangerate.R;
import ru.alexeyzhulin.bitcoinexchangerate.interfaces.ICurrencyRefreshSubscriber;
import ru.alexeyzhulin.bitcoinexchangerate.models.AsyncTaskResult;
import ru.alexeyzhulin.bitcoinexchangerate.models.Currency;
import ru.alexeyzhulin.bitcoinexchangerate.models.CurrencyHistoryParam;
import ru.alexeyzhulin.bitcoinexchangerate.models.CurrencyRate;
import ru.alexeyzhulin.bitcoinexchangerate.models.ValueDirection;
import ru.alexeyzhulin.bitcoinexchangerate.services.CurrencyHelper;

import static ru.alexeyzhulin.bitcoinexchangerate.models.SortDirection.DESC;

/**
 * Created by Alex on 29.10.2017.
 */

public class HistoryFragment extends Fragment implements ICurrencyRefreshSubscriber {

    private ProgressBar progressBar;
    private ListView listView;

    private View view;
    private Currency currency;
    private ArrayList<CurrencyRate> totalCurrencyRates = new ArrayList<>();
    private ArrayList<HashMap<String, Object>> adapterValues = new ArrayList<>();
    private SimpleAdapter adapter;

    private Boolean isLoading = false;
    private Boolean nextPageExists = true;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_history, container, false);

        // Set listener for currency spinner change selection
        if (getActivity() instanceof MainActivity) {
            ((MainActivity) getActivity()).setDataRefresher(this);
        }

        progressBar = view.findViewById(R.id.progressBar);
        progressBar.setVisibility(View.GONE);

        listView = view.findViewById(R.id.listView);
        adapter = new SimpleAdapter(getContext(), adapterValues,
                R.layout.list_item_history, new String[]{"historyDate", "historyRate", "deltaValue"},
                new int[]{R.id.historyDate, R.id.historyRate, R.id.deltaValue});
        listView.setAdapter(adapter);

        listView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView absListView, int i) {
            }

            @Override
            public void onScroll(AbsListView absListView, int firstVisibleItem, int visibleItemCount,
                                 int totalItemCount) {
                final int lastInScreen = visibleItemCount + firstVisibleItem;
                if (lastInScreen == totalItemCount && totalItemCount > 0 && !isLoading && nextPageExists) {
                    // Get next period
                    Date lastDate = totalCurrencyRates.get(totalItemCount - 1).getRateDate();
                    CurrencyHistoryParam currencyHistoryParam = CurrencyHelper.getNextCurrencyHistoryParam(currency, lastDate);

                    progressBar.setVisibility(View.VISIBLE);
                    new getCurrencyRateHistory().execute(currencyHistoryParam);
                }
            }
        });

        return view;
    }

    @Override
    public void refreshData() {
        CurrencyHistoryParam currencyHistoryParam = new CurrencyHistoryParam();
        currencyHistoryParam.setCurrency(currency);

        totalCurrencyRates.clear();
        adapterValues.clear();
        adapter.notifyDataSetChanged();

        progressBar.setVisibility(View.VISIBLE);
        if (!isLoading) {
            new getCurrencyRateHistory().execute(currencyHistoryParam);
        }
    }

    @Override
    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    private class getCurrencyRateHistory extends AsyncTask<CurrencyHistoryParam, Integer, AsyncTaskResult<ArrayList<CurrencyRate>>> {

        @Override
        protected AsyncTaskResult<ArrayList<CurrencyRate>> doInBackground(CurrencyHistoryParam... currencyHistoryParams) {
            try {
                isLoading = true;
                return new AsyncTaskResult<>(CurrencyHelper.getCurrencyRateHistory(currencyHistoryParams[0], DESC));
            } catch (Exception e) {
                return new AsyncTaskResult<>(e);
            }
        }

        @Override
        protected void onPostExecute(AsyncTaskResult<ArrayList<CurrencyRate>> result) {
            if (result.getError() != null) {
                // error handling here
                //Toast.makeText(view.getContext(), R.string.connection_error, Toast.LENGTH_LONG).show();
                Toast.makeText(view.getContext(), result.getError().getMessage(), Toast.LENGTH_LONG).show();
            } else if (isCancelled()) {
                Toast.makeText(view.getContext(), R.string.request_canceled, Toast.LENGTH_LONG).show();
            } else {
                final ArrayList<CurrencyRate> currencyRates = result.getResult();
                totalCurrencyRates.addAll(currencyRates);
                nextPageExists = currencyRates.size() > 0;

                DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

                DecimalFormat formatter = new DecimalFormat("#,###.0000");
                DecimalFormatSymbols symbols = formatter.getDecimalFormatSymbols();
                symbols.setGroupingSeparator(' ');
                formatter.setDecimalFormatSymbols(symbols);

                DecimalFormat formatterDelta = new DecimalFormat("#,#0.00");
                DecimalFormatSymbols symbolsDelta = formatter.getDecimalFormatSymbols();
                symbolsDelta.setGroupingSeparator(' ');
                formatterDelta.setDecimalFormatSymbols(symbolsDelta);

                for (CurrencyRate currencyRate : currencyRates
                        ) {
                    HashMap<String, Object> value = new HashMap<>();
                    value.put("historyDate", dateFormat.format(currencyRate.getRateDate()));
                    value.put("historyRate", formatter.format(currencyRate.getRateValue()));
                    if (currencyRate.getValueDirection() == ValueDirection.UP) {
                        value.put("deltaValue", getString(R.string.up_pointing_triangle) + " " + formatterDelta.format(currencyRate.getDelta()) + "%");
                    } else if (currencyRate.getValueDirection() == ValueDirection.DOWN) {
                        value.put("deltaValue", getString(R.string.down_pointing_triangle) + " " + formatterDelta.format(currencyRate.getDelta()) + "%");
                    } else {
                        value.put("deltaValue", "");
                    }

                    adapterValues.add(value);
                }
                adapter.notifyDataSetChanged();
            }
            progressBar.setVisibility(View.GONE);
            isLoading = false;
        }
    }
}
