package ru.alexeyzhulin.bitcoinexchangerate.activities;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Spinner;
import android.widget.Toast;

import java.io.IOException;
import java.util.ArrayList;

import ru.alexeyzhulin.bitcoinexchangerate.R;
import ru.alexeyzhulin.bitcoinexchangerate.interfaces.ICurrencyRefreshSubscriber;
import ru.alexeyzhulin.bitcoinexchangerate.models.AsyncTaskResult;
import ru.alexeyzhulin.bitcoinexchangerate.models.Currency;
import ru.alexeyzhulin.bitcoinexchangerate.models.CurrencyListAdapter;
import ru.alexeyzhulin.bitcoinexchangerate.services.CurrencyHelper;

/**
 * Created by Alex on 19.10.2017.
 */

public class MainActivity extends AppCompatActivity {

    private static Integer COUNT_PAGES = 3;

    private Spinner currencySpinner;

    private AppSectionsPagerAdapter appSectionsPagerAdapter;
    private ViewPager viewPager;

    private ArrayList<ICurrencyRefreshSubscriber> dataRefreshSubscribers = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        appSectionsPagerAdapter = new AppSectionsPagerAdapter(getSupportFragmentManager());

        viewPager = (ViewPager) findViewById(R.id.pager);
        viewPager.setAdapter(appSectionsPagerAdapter);
        viewPager.setOffscreenPageLimit(COUNT_PAGES);

        currencySpinner = (Spinner) findViewById(R.id.currency);
        currencySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                Currency currency = (Currency) currencySpinner.getSelectedItem();
                try {
                    CurrencyHelper.saveCurrencyData(view.getContext().getFilesDir().toString(), currency);
                    //new getCurrencyRate().execute(currency);
                    for (ICurrencyRefreshSubscriber subscriber : dataRefreshSubscribers
                            ) {
                        subscriber.setCurrency(currency);
                        subscriber.refreshData();
                    }
                } catch (IOException e) {
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        new fillCurrencyList().execute();

    }

    public void setDataRefresher(ICurrencyRefreshSubscriber refreshListener) {
        dataRefreshSubscribers.add(refreshListener);
    }

    public class AppSectionsPagerAdapter extends FragmentPagerAdapter {

        public AppSectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int i) {
            Fragment fragment;
            switch (i) {
                case 0:
                    fragment = new CurrentRateFragment();
                    break;
                case 1:
                    fragment = new HistoryFragment();
                    break;
                case 2:
                    fragment = new GraphFragment();
                    break;
                default:
                    fragment = new CurrentRateFragment();
                    break;
            }
            ((ICurrencyRefreshSubscriber) fragment).setCurrency((Currency) currencySpinner.getSelectedItem());

            return fragment;
        }

        @Override
        public int getCount() {
            return COUNT_PAGES;
        }
    }

    private class fillCurrencyList extends AsyncTask<Integer, Integer, AsyncTaskResult<ArrayList<Currency>>> {

        @Override
        protected AsyncTaskResult<ArrayList<Currency>> doInBackground(Integer... params) {
            try {
                return new AsyncTaskResult<>(CurrencyHelper.getCurrencyList());
            } catch (Exception e) {
                return new AsyncTaskResult<>(e);
            }
        }

        @Override
        protected void onPostExecute(AsyncTaskResult<ArrayList<Currency>> result) {
            if (result.getError() != null) {
                // error handling here
                //Toast.makeText(view.getContext(), R.string.connection_error, Toast.LENGTH_LONG).show();
                Toast.makeText(getApplicationContext(), result.getError().getMessage(), Toast.LENGTH_LONG).show();
            } else if (isCancelled()) {
                Toast.makeText(getApplicationContext(), R.string.request_canceled, Toast.LENGTH_LONG).show();
            } else {
                final ArrayList<Currency> currencies = result.getResult();
                Currency[] currenciesArr = new Currency[currencies.size()];
                currencies.toArray(currenciesArr);
                CurrencyListAdapter adapter = new CurrencyListAdapter(getApplicationContext(),
                        android.R.layout.simple_spinner_item,
                        currenciesArr);
                currencySpinner.setAdapter(adapter);
                currencySpinner.post(new Runnable() {
                    @Override
                    public void run() {
                        Currency currency;
                        try {
                            currency = CurrencyHelper.loadCurrencyData(getApplicationContext().getFilesDir().toString());
                        } catch (Exception e) {
                            currency = new Currency();
                            currency.setCode(CurrencyHelper.USD_CURRENCY_CODE);
                            currency.setName(CurrencyHelper.USD_CURRENCY_NAME);
                        }
                        if (currencies.indexOf(currency) >= 0) {
                            currencySpinner.setSelection(currencies.indexOf(currency));
                        } else {
                            currencySpinner.setSelection(0);
                        }
                    }
                });
            }
        }
    }

}
